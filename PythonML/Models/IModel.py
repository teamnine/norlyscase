import abc

class IModel(metaclass=abc.ABCMeta):
    #@abc.abstractproperty
    #def model(self):
    #    return None

    @abc.abstractmethod
    def predict(self, input_values):
        pass
    @abc.abstractmethod
    def train(self, train_data):
        pass

