from Models.IModel import IModel
from sklearn import ensemble

class GradientBoostingRegressionModel(IModel):
    def __init__(self):
        self.model = ensemble.GradientBoostingRegressor(alpha=0.85, learning_rate=0.1, loss="huber", max_depth=3, max_features=1.0, min_samples_leaf=12, min_samples_split=7, n_estimators=100, subsample=0.6500000000000001)

    def predict(self, input_values):
        return self.model.predict(input_values)

    def train(self, train_data, test_data=None):
        if test_data is None:
            return self.model.fit(train_data)
        return self.model.fit(train_data, test_data)