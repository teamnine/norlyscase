import pandas as pd
import numpy as np
import pymssql
import time
# KNN imports
from sklearn import ensemble
from Models.GBR_Model import GradientBoostingRegressionModel

# Some other example server values are
# server = 'localhost\sqlexpress' # for a named instance
# server = 'myserver,port' # to specify an alternate port
# db_name: the ucn number of the database
def get_database_connection(db_name):
    server = 'hildur.ucn.dk' 
    db = db_name
    username = db_name
    password = 'Password1!' 
    return pymssql.connect(server = server, user = username, password = password, database = db)

# import data from online_data
# file_name: the name of the csv file
def import_data(file_name, headers):
    # import
    data = pd.read_csv(f'AssetsData/{file_name}.csv', sep=',', parse_dates=['time_utc']) 
    df = pd.DataFrame(data)
    df = df[df['active_power_watt'] > 0]

    # set titles
    df.columns = headers

    # round the time to 30 seconds
    df['TimeUTC'] = df['TimeUTC'].round('30s')
    return df

dfs = {}
# get db connection
connection = get_database_connection('dmai0920_1028757')
cursor = connection.cursor()

#create a regression model
df = pd.read_csv('AssetsData/formatted_data_march.csv', sep=',', parse_dates=['time_utc'], dtype=np.float64)
df.columns = ['TimeUTC', 'AssetId', 'WindSpeedMs', 'WindDirectionDegrees', 'Temperature', 'ActivePowerWatt']
df['TimeUTC'] = df['TimeUTC'].round('30s')
df = df[df['ActivePowerWatt'] > 0]
assetId = 47
asset_train_data = df[df['AssetId'] == assetId]

cond1 = (asset_train_data.TimeUTC < pd.Timestamp(2022,3,9,13,30)) | (asset_train_data.TimeUTC > pd.Timestamp(2022,3,10,0,0))
asset_train_data = asset_train_data.loc[cond1]

training_features = asset_train_data[['WindSpeedMs', 'WindDirectionDegrees', 'Temperature']]
training_target = asset_train_data['ActivePowerWatt']
regressor = GradientBoostingRegressionModel()
print(f'[ML] Training {regressor.model.__class__.__name__}...')
train_start_time = time.time()
train_result = regressor.train(training_features, training_target)
train_end_time = time.time()
print(f'[ML] {regressor.model.__class__.__name__} model trained...\n\
    * Time elappsed: {train_end_time - train_start_time} seconds\n\
    * Result: {train_result}')

def get_tuple_from_row(row, delta):
    row[0], row[1] = row[1], row[0] # swap time and id
    #converts id from np.int64 to python int, otherwise the INSERT won't work
    row[0] = row[0].item()
    row.insert(2, float(0)) #temperature
    row.insert(2, float(0)) #wind_direction_degrees
    row.insert(2, float(0)) #wind_speed_ms
    row[5] = float(0)       #active_power_watt
    row[6] += delta         #predicted_active_power_watt
    row.append(float(0))    #predicted_active_power_watt_knn
    return tuple(row)

def start_prediction_forecast(last_row):
    global cursor
    global connection
    #get the df for that asset
    df = get_data_frames(ids=[last_row[0]], headers=['TimeUTC', 'AssetId', 'ActivePowerWatt', 'DummyActivePowerWatt'])[last_row[0]]
    index = df.index[df['TimeUTC'] == last_row[1]]
    row = df.loc[index].values[0]
    #delta = active_power - dummy_active_power
    delta = last_row[5] - row[3]
    print(f'DELTA: {delta}')

    # Use delta to predict
    index = index[0]
    values = []
    for index in range(index, index + 50):
        values.append(get_tuple_from_row(list(df.loc[index].values), delta))

    # insert
    #'AssetId', 'TimeUTC', 'WindSpeedMs', 'WindDirectionDegrees', 'Temperature', 'ActivePowerWatt', 'PredictedActivePowerWatt', 'PredictedActivePowerWattKNN'
    print(f'[INSERTING PREDICTIONS] Inserted {len(values)} predictions using Forecast')
    cursor.executemany("INSERT INTO OnlineData VALUES (%s,%s,%s,%s,%s,%s,%s,%s)", values)
    connection.commit()
    print('[INSERTING PREDICTIONS] Done...')

# gives neighbors for asset 47
def getNearestNeighbors(assetId):  
    return pd.DataFrame({
        'id': assetId,
        'neighbor1': 49,
        'neighbor2': 50,
        'neighbor3': 54,
    }, index=[0], columns=['id', 'neighbor1', 'neighbor2', 'neighbor3'])

def close_connection():
    global connection
    connection.commit()
    connection.close()

def start_prediction_knn(last_row):
    global connection
    global cursor
    global regressor
    global df
    #import the data

    neighbors = getNearestNeighbors(47)
    n_df = pd.DataFrame(df.loc[df['AssetId'].isin(neighbors['neighbor1'])])
    n_df = pd.concat([n_df, pd.DataFrame(df.loc[df['AssetId'].isin(neighbors['neighbor2'])])])
    n_df = pd.concat([n_df, pd.DataFrame(df.loc[df['AssetId'].isin(neighbors['neighbor3'])])])

    #round the time to 30 s and remove duplicates
    n_df = n_df.set_index('TimeUTC')
    n_df = n_df.sort_index()
    n_df.index = n_df.index.round('30s')

    time = last_row[1]
    n_df1 = n_df[n_df.index == time]
    df_id = df.loc[(df.AssetId.isin([assetId])) & (df.TimeUTC == time)]
    if len(n_df1) >= 2 and len(df_id) > 0:
        wind_speed = n_df1['WindSpeedMs'].mean()
        wind_direction = n_df1['WindDirectionDegrees'].mean()
        temperature = n_df1['Temperature'].mean()

        mean_df = pd.DataFrame({'WindSpeedMs': [wind_speed], 'WindDirectionDegrees': [wind_direction], 'Temperature': [temperature]})
        result = regressor.predict(mean_df)
        
        actual_test = df_id.iloc[0]['ActivePowerWatt']

        cursor.execute("UPDATE OnlineData SET PredictedActivePowerWattKNN=%s WHERE AssetId=%s AND TimeUTC=%s", (result[0], 47, time))
        connection.commit()
        print(f'[KNN] Predicted {result[0]}/{actual_test}')
    else:
        print(f'[KNN] couldnt find neighbors for the timestamp {time}.')

# get a dataframe for each id
# ids: the ids which it should import
# reimport: bool for reimport the dfs
def get_data_frames(headers, ids = [47, 49, 50, 54, 93, 94, 96, 97], file_name='dummy_forecast_9march_id', reimport = False):
    # import the forecasts
    global dfs
    if not dfs or not reimport:
        for id in ids:
            dfs[id] = import_data(f'{file_name}{id}', headers)
    return dfs