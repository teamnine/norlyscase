import pandas as pd
import numpy as np
import pymssql
import time
import sys, os
import pynput.keyboard as keyboard
from PowerPredictor import close_connection, start_prediction_forecast
from PowerPredictor import start_prediction_knn

# import data from online_data
# file_name: the name of the csv file
def import_data(file_name): 
    # import
    headers = ['TimeUTC', 'AssetId', 'WindSpeedMs', 'WindDirectionDegrees', 'Temperature', 'ActivePowerWatt']
    data = pd.read_csv(f'AssetsData/{file_name}.csv', parse_dates=['TimeUTC'], names=headers, sep=',', skiprows=1) 
    df = pd.DataFrame(data)

    # set titles & add predicted active power
    df = df[['AssetId', 'TimeUTC', 'WindSpeedMs', 'WindDirectionDegrees', 'Temperature', 'ActivePowerWatt']]
    df.insert(6, "PredictedActivePowerWatt", 0)
    df.insert(7, "PredictedActivePowerWattKNN", 0)

    # round the time to 30 seconds
    df['TimeUTC'] = df['TimeUTC'].round('30s')
    return df

# Some other example server values are
# server = 'localhost\sqlexpress' # for a named instance
# server = 'myserver,port' # to specify an alternate port
# db_name: the ucn number of the database
def get_database_connection(db_name):
    servr = 'hildur.ucn.dk' 
    db = db_name
    username = db_name
    password = 'Password1!' 
    return pymssql.connect(server = servr, user = username, password = password, database = db)
# get db connection
connection = get_database_connection('dmai0920_1028757')
cursor = connection.cursor()
#cursor.fast_executemany = True

# Insert a row into the DB
# row: a single row from a DF with the right headers
def insert_row(row):
    global connection
    global cursor
    if isinstance(row, list):
        try:
            cursor.executemany("INSERT INTO OnlineData VALUES (%s,%s,%s,%s,%s,%s,%s,%s)", row)
            print(f"[INSERTED] {len(row)} previously skipped rows!")
        except:
            # change ID and Time to be at the end as the query requires it
            index = 0
            for tuple in row:
                row[index] = tuple[2:6] + tuple[:2] #organize the tuple emelents to match the update query
                index += 1
            cursor.executemany("UPDATE OnlineData SET WindSpeedMs=%s, WindDirectionDegrees=%s, Temperature=%s, ActivePowerWatt=%s WHERE AssetId=%s AND TimeUTC=%s", row)
            print(f"+ [Updated] {len(row)} previously skipped rows!")
    else:
        if not isinstance(row, np.ndarray):
            row = row.to_numpy()
        try:
            cursor.execute("INSERT INTO OnlineData VALUES (%s,%s,%s,%s,%s,%s,%s,%s)", (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7]))
            print(f"[INSERTED] Row: {row}", end='\n')
        except:
            cursor.execute("UPDATE OnlineData SET WindSpeedMs=%s, WindDirectionDegrees=%s, Temperature=%s, ActivePowerWatt=%s WHERE AssetId=%s AND TimeUTC=%s", (row[2], row[3], row[4], row[5], row[0], row[1]))
            print(f"+ [UPDATED+] Row: {row}", end='\n')
    connection.commit()

# Import all data for the 8th of march all at once
def bulk_import(data_frame):
    try:
        # Selects only 1 day of data
        cond1 = (data_frame.TimeUTC > pd.Timestamp(2022,3,9,9,0)) & (data_frame.TimeUTC < pd.Timestamp(2022,3,9,13,30))
        data_frame = data_frame.loc[cond1]
        data_frame.reset_index(drop=True, inplace=True)
        data_frame = data_frame.sort_values(by='TimeUTC')
        print('Inserting the 8th of March (this will take a while)...')
        for index, row in data_frame.iterrows():
            try:
                if exit_loop:
                    return
                insert_row(row)
            except Exception as e:
                # If there is a duplicate row just skip it
                pass
    except KeyboardInterrupt: 
        return

# Delete the new insertions from the 9th added by 'gradual_insert()'
def delete_insertions():
    global connection
    global cursor
    cursor.execute("DELETE FROM OnlineData WHERE TimeUTC >= '2022-3-9 13:30'")
    connection.commit()

# splits the df into many dfs by id
def get_id_split_dataframes(data_frame):
    list_data_frames = []
    for id in [47, 49, 50, 54, 93, 94, 96, 97]:
        current_df = data_frame[data_frame['AssetId']==id].sort_values(by='TimeUTC')
        current_df.reset_index(drop=True, inplace=True)
        list_data_frames.append(current_df)
    return list_data_frames

# Define keypress listener
exit_loop = False
fallout_started = False
def on_press(key):
    global exit_loop
    global fallout_started
    try:
        if key == keyboard.Key.esc:
            exit_loop = True
        elif key.char == 'f':
            fallout_started = not fallout_started
    except:
        pass
listener = keyboard.Listener(on_press=on_press)
listener.start()

# gradually inserts 1 row per asset every second
def gradual_insert(data_frame):
    global exit_loop
    global fallout_started
    # Selects only 1 day of data
    cond1 = (df.TimeUTC >= pd.Timestamp(2022,3,9,13,30)) & (df.TimeUTC < pd.Timestamp(2022,3,10,0,0))
    data_frame = data_frame.loc[cond1]
    data_frame.reset_index(drop=True, inplace=True)
    index = 0
    already_in_fallout = False
    # Start the loop
    skipped_rows = []
    try:
        while True:
            for current_data_frame in get_id_split_dataframes(data_frame):
                row = current_data_frame.loc[[index]].values[0]
                try:
                    if exit_loop:
                        return
                    if fallout_started and row[0] == 47:
                        # at the end of the fallout add that list back the the db for comparisons reasons later
                        print(f"[SKIPPED] Row: {index}, {row}", end='\n')
                        row = tuple(row)
                        skipped_rows.append(row)
                        if not already_in_fallout:
                            already_in_fallout = True
                            start_prediction_forecast(row)
                            #time.sleep(10)
                        start_prediction_knn(row)
                    else:
                        if row[0] == 47:
                            already_in_fallout = False
                            if skipped_rows:
                                insert_row(skipped_rows)
                                skipped_rows.clear()
                        insert_row(row)
                except Exception as e:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    print(f'Error on line {exc_tb.tb_lineno}({os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]}): ' + str(e))
                    pass
            index += 1
            time.sleep(0.2)
    except KeyboardInterrupt: 
        return

# import
df = import_data('formatted_data_march')

option = input("""
Enter '1' or '2':
 1) Populate the 8th of March at once
 2) Start inserting the 9th every second per asset & then cleanup what was inserted at the end
(Press 'Enter' to stop either of them while running...)
 :""")

if option == '1':
    bulk_import(df)
    print('Done...')
    exit_loop = False
elif option == '2':
    gradual_insert(df)
    delete_insertions()
    print('Deleted inserted rows')
    exit_loop = False

connection.commit()
connection.close()

close_connection()