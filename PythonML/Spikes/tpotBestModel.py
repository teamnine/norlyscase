import numpy as np
import pandas as pd
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.linear_model import RidgeCV
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline, make_union
from tpot.builtins import StackingEstimator
from tpot.export_utils import set_param_recursive

# NOTE: Make sure that the outcome column is labeled 'active_power_watt' in the data file
tpot_data = pd.read_csv('../AssetsData/formatted_data_march.csv', sep=',', dtype=np.float64)
features = tpot_data.drop('active_power_watt', axis=1)
training_features, testing_features, training_target, testing_target = \
            train_test_split(features, tpot_data['active_power_watt'], random_state=1)

# Average CV score on the training set was: 0.8821633815645982
exported_pipeline = make_pipeline(
    StackingEstimator(estimator=RidgeCV()),
    GradientBoostingRegressor(alpha=0.85, learning_rate=0.1, loss="huber", max_depth=3, max_features=1.0, min_samples_leaf=12, min_samples_split=7, n_estimators=100, subsample=0.6500000000000001)
)
# Fix random state for all the steps in exported pipeline
set_param_recursive(exported_pipeline.steps, 'random_state', 1)

exported_pipeline.fit(training_features, training_target)
results = exported_pipeline.predict(testing_features)  