import pandas as pd
from flask import Flask, jsonify
import time

#import data
headers = ['time_utc', 'id', 'wind_speed_ms', 'wind_direction_degrees', 'temperature', 'active_power_watt']

df = pd.read_csv("../../AssetsData/formatted_data_march.csv", sep=',', names=headers, skiprows=1)

df.sort_values("time_utc", inplace=True)

app = Flask(__name__)
counter = 0

@app.route('/data/<id>', methods=['GET'])
def get_data_for_asset(id):
    asset_data = df[df.id == int(id)]
    asset_data.reset_index(drop=True, inplace=True)
    global counter
    counter += 1
    return jsonify({'data': [asset_data.iloc[counter-1].to_dict()]})

app.run(port=7001, debug=True)