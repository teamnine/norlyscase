import random
from bokeh.driving import count
from bokeh.models import ColumnDataSource
from bokeh.events import ButtonClick
from bokeh.plotting import curdoc, figure
from bokeh.models import Panel, Tabs, Button
from bokeh.colors import RGB
import pandas as pd
import requests

update_interval = 20
roll_over = 500
source = ColumnDataSource({'x':[], 'y':[]})


# reading in the data
headers = ['time_utc','id','wind_speed_ms','wind_direction_degrees','temperature','active_power_watt']

df = pd.read_csv("../../AssetsData/formatted_data_march.csv", sep=',', names=headers, parse_dates=['time_utc'], skiprows=1)
print(df.head())

df = df[df.id == 47]
df.sort_values("time_utc", inplace=True)

@count()
def update(x):
    y = 0
    if x < len(df):
        y = df.iloc[x]['active_power_watt']
        #print(y)
    else:
        y = random.random()
    source.stream({'x':[df.iloc[x]['time_utc']], 'y':[y]}, rollover=roll_over)
    #source.stream({'x':[x], 'y':[y]}, rollover=roll_over)

color = RGB(200, 100, 150)

p1 = figure(width=1200, height=600)
plot_line = p1.line('x', 'y', source=source, color=color)
p1.xaxis.axis_label = 'Time UTC'
p1.yaxis.axis_label = 'Active Power (Watt)'
tab1 = Panel(child=p1, title='line')


from bokeh.models import DatetimeTickFormatter

p1.xaxis.formatter = DatetimeTickFormatter(
    days=["%m/%d %H:%M"],
    months=["%m/%d %H:%M"],
    hours=["%m/%d %H:%M"],
    minutes=["%m/%d %H:%M"]
)
p2 = figure(width=1200, height=600)
p2.circle('x', 'y', source=source)
p2.xaxis.axis_label = 'x'
p2.yaxis.axis_label = 'y'
tab2 = Panel(child=p2, title='circle')

def callback_for_stranger_button():
    p1.color = RGB(0, 200, 200)

button = Button(label='Hi yes Starger!', button_type='danger')
button.on_event(ButtonClick, callback_for_stranger_button)

from bokeh.models import Select

def select_asset(attr, old, id):
    response = requests.get(f"http://localhost:7001/data/{int(id)}")
    print(id)
    if id == "47":
        print(response.json())
    asset_data = source.stream(response.json()["data"][0], rollover=roll_over)
    plot_line = p1.select_one({'name': 'plot_line'})
    plot_line.visible = False
    p1.line('x', 'y', source=asset_data, color="blue")
    p1.xaxis.axis_label = 'Time UTC'
    p1.yaxis.axis_label = 'Active Power (Watt)'

select = Select(title="Select wind turbine:", value="49", options=["47", "49", "50", "54", "93", "94", "96", "97"])
select.on_change('value', select_asset)

doc = curdoc()

doc.add_root(button)
doc.add_root(select)
doc.add_root(Tabs(tabs=[tab1, tab2]))
doc.add_periodic_callback(update, update_interval)
#to run write in cmd: bokeh serve --show "path to this file"
import os
os.system(f'bokeh serve --show {__file__}')