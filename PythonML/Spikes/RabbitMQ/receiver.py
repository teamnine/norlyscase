#!/usr/bin/env python
import pika
import time
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.exchange_declare(exchange='router', exchange_type='direct')

result = channel.queue_declare(queue='', exclusive=True)
queue_name = result.method.queue

channel.queue_bind(exchange='router', queue=queue_name, routing_key='47')

data_power = []
data_time = []
#df = pd.DataFrame(columns=['active_power_watt', 'time_utc'])

def callback(ch, method, properties, body):
    #print(" [x] Received %r" % body.decode())
    #time.sleep(body.count(b'.'))
    #print(" [x] Done")
    b = body.decode()
    ch.basic_ack(delivery_tag=method.delivery_tag)
    if b == 'done':
        channel.stop_consuming()
    else:
        #df.loc[len(df)] = b.split(',')
        active_power_watt, time_utc = b.split(',')
        data_power.append(active_power_watt)
        data_time.append(time_utc)

channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue=queue_name, on_message_callback=callback)

channel.start_consuming()

df = pd.DataFrame({'time_utc': data_time, 'active_power_watt': data_power})
df['time_utc'] = pd.to_datetime(df['time_utc'])

sns.lineplot(df['time_utc'], df['active_power_watt'])
plt.show()

connection.close()
