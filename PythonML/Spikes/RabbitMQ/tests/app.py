from flask import Flask, config, render_template, request
import pandas as pd
import json
import plotly
import plotly.express as px

app = Flask(__name__)

@app.route('/callback', methods=['POST', 'GET'])
def cb():
    return gm(request.args.get('data'))
   
@app.route('/')
def index():
    return render_template('chartsajax.html',  graphJSON=gm())

def gm(country='United Kingdom'):
    df = pd.DataFrame(px.data.gapminder())
    
    #df_c = df[~df['country'].duplicated()]['country']
    #df_c.reset_index(drop=True, inplace=True)
    #print(df_c)

    fig = px.line(df[df['country']==country], x="year", y="lifeExp")
    
    
    

    graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
    #print(fig.data[0])
    #fig.data[0]['staticPlot']=True
    
    return graphJSON

if __name__ == '__main__':
    app.run()
