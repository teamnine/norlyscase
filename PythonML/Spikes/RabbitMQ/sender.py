import pandas as pd, pika as pika, sys, time
import matplotlib as plt
from tqdm import tqdm


headers = ['time_utc', 'external_mapping_id', 'active_power_watt','possible_power_watt','wind_speed','rpm','active_stop','control_state_id','online_data_expired','wind_direction','temperature','active_stop_user_id','received_time_utc']

parse_dates = ['time_utc', 'received_time_utc']
df = pd.read_csv("../../online_data/new_online_data.csv", sep=',', names=headers, parse_dates=parse_dates, skiprows=1)
df = df[['time_utc','active_power_watt', 'wind_speed', 'external_mapping_id']]
#df.set_index('time_utc', inplace=True)
df.sort_values(by='time_utc', inplace=True)

#print(df.head())

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.exchange_declare(exchange='router', exchange_type='direct')

messages = ['hey', 'wassup', 'shit', 'garbage'] * 100

for row in tqdm(df[:50000].itertuples()):
    channel.basic_publish(
        exchange='router',
        routing_key=str(row.external_mapping_id),
        body=f'{row.active_power_watt},{row.time_utc}',
        properties=pika.BasicProperties(delivery_mode=pika.spec.PERSISTENT_DELIVERY_MODE)
    )

channel.basic_publish(
        exchange='router',
        routing_key='47',
        body='done',
        properties=pika.BasicProperties(delivery_mode=pika.spec.PERSISTENT_DELIVERY_MODE)
    )

'''
for message in messages:
    #message = ' '.join(sys.argv[1:]) or "Hello World!"
    channel.basic_publish(
        exchange='',
        routing_key='task_queue',
        body=message,
        properties=pika.BasicProperties(
            delivery_mode=pika.spec.PERSISTENT_DELIVERY_MODE
        ))
    print(" [x] Sent %r" % message)
connection.close()
'''