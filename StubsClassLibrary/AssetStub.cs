﻿using DataAccess.Interfaces;
using DataAccess.Models;

namespace StubsClassLibrary
{
    public class AssetStub : IAssetRepository
    {
        public async Task<int> CreateAsync(Asset entity)
        {
            return await Task.FromResult(entity.Id);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            return await Task.FromResult(true);
        }

        public async Task<IEnumerable<Asset>> GetAllAsync()
        {
            return await Task.FromResult(new List<Asset>()
            {
                new Asset() { Id = 1, Neighbor1 = 2, Neighbor2 = 3, Neighbor3 = 4 },
                new Asset() { Id = 2, Neighbor1 = 1, Neighbor2 = 3, Neighbor3 = 4 },
                new Asset() { Id = 3, Neighbor1 = 2, Neighbor2 = 4, Neighbor3 = 1 }
            });
        }

        public async Task<Asset> GetByIdAsync(int id)
        {
            Asset asset = new Asset() { Id = id, Neighbor1 = 1, Neighbor2 = 2, Neighbor3 = 3 };
            return await Task.FromResult(asset);
        }

        public async Task<bool> UpdateAsync(Asset entity)
        {
            return await Task.FromResult(true);
        }
    }
}