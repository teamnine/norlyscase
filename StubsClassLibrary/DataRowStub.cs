﻿using DataAccess.Interfaces;
using DataAccess.Models;

namespace StubsClassLibrary
{
    public class DataRowStub : IDataRowRepository
    {
        public async Task<int> CreateAsync(DataRow entity)
        {
            return await Task.FromResult(1);
        }

        public async Task<IEnumerable<DataRow>> GetAllAsync()
        {
            return await Task.FromResult(new List<DataRow>()
            {
                new DataRow() {  },
                new DataRow() {  },
                new DataRow() {  }
            });
        }

        public async Task<IEnumerable<DataRow>> GetByIdAsync(int id)
        {
            return await Task.FromResult(new List<DataRow>()
            {
                new DataRow() { TimeUTC = new DateTime(2022, 2, 1) },
                new DataRow() { TimeUTC = new DateTime(2022, 2, 1) },
                new DataRow() { TimeUTC = new DateTime(2022, 2, 1) }
            });
        }

        public async Task<bool> UpdateAsync(DataRow entity)
        {
            return await Task.FromResult(true);
        }
    }
}
