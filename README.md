## Reneweable Energy Forecasting
> A project focusing on a ML approach when solving a Renewable energy foreacting issue of an asset loosing connection and an appropriate forecasting model is needed.
> <!-- Live demo [_here_](https://www.example.com).  After maybe hosting osme of the functionality, include the link here. -->

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
<!--* [Screenshots](#screenshots)-->
* [Setup](#setup)
* [Usage](#usage)
* [Project Status](#project-status)
* [Room for Improvement](#room-for-improvement)
* [Acknowledgements](#acknowledgements)
* [Contact](#contact)
<!-- * [License](#license) -->


## General Information
- Norlys Enegry Trading is used as a case study
- What problem does it (intend to) solve?
- What is the purpose of your project?
- Why did you undertake it?
<!-- You don't have to answer all the questions - just the ones relevant to your project. -->


## Technologies Used
- C#, SQL, Python
- Dapper & AutoMapper - faster SQL communication and model mapping
- Bokeh - used for visualization


## Features
As of currently:
- Mulitple Machine Learning models finished
- Recreating a fallout
- Proof-Of-Concept architecture

<!--
## Screenshots
![Example screenshot](./img/screenshot.png)
<!-- If you have screenshots you'd like to share, include them here. -->


## Setup
Currently, no requirements.txt file has been established, but will be considered for the future.

## Usage
This is a Machine Learning strategy exploration as well as a proposed architecture and a simple Proof-Of-Concept. Current solution requires sever side running as well as a web client and a desktop client.


## Project Status
Project is currently _in progress_. Core functionality is being established and possible bugs and issues can occur.


## Room for Improvement
Room for improvement:
- Current implementation can be considired slower as it is implemented in Python

To do:
- Improve Machine Learning Model Strategy
- Improve Machine Learning Model Strategy


## Acknowledgements
The project was a purely original creation, that is definitely not a trivial solution to an already largely discussed issue.


## Contact
Created by Team 9 dmaai0920, UCN - feel free to contact any of us!


<!-- Optional -->
<!-- ## License -->
<!-- This project is open source and available under the [... License](). -->

<!-- You don't have to include all sections - just the one's relevant to your project -->
