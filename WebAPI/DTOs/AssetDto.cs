namespace WebAPI
{
    public class AssetDto
    {
        public int Id { get; set; }

        public int Neighbor1 { get; set; }

        public int Neighbor2 { get; set; }

        public int Neighbor3 { get; set; }
    }
}