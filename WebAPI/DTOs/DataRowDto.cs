﻿namespace WebAPI.DTOs
{
    public class DataRowDto
    {
        public int AssetId { get; set; }
        public DateTime TimeUTC { get; set; }
        public int WindSpeedMs { get; set; }
        public int WindDirectionDegrees { get; set; }
        public int Temperature { get; set; }
        public int ActivePowerWatt { get; set; }
        public int PredictedActivePowerWatt { get; set; }
        public int PredictedActivePowerWattKNN { get; set; }
    }
}
