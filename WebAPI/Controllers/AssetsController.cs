﻿using Microsoft.AspNetCore.Mvc;
using DataAccess.Interfaces;
using DataAccess.Models;
using WebAPI.DTOs.Converters;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AssetsController : ControllerBase
    {
        IAssetRepository _assetRepository;
        // GET: api/<AssetsController>

        public AssetsController(IAssetRepository assetRepository)
        {
            _assetRepository = assetRepository;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<AssetDto>>> GetAllAsync()
        {
            //TODO: add if cases for return NotFound() or anything else
            var assets = await _assetRepository.GetAllAsync();
            var assetDtos = DtoConverter<Asset, AssetDto>.FromList(assets);
            return Ok(assetDtos);
        }

        // GET api/<AssetsController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AssetDto>> GetByIdAsync(int id)
        {

            //TODO: add if cases for return NotFound() or anything else
            var asset = await _assetRepository.GetByIdAsync(id);
            var assetDto = DtoConverter<Asset, AssetDto>.From(asset);
            return Ok(assetDto);
        }

        // POST api/<AssetsController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<AssetsController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<AssetsController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
