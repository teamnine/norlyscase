﻿using DataAccess.Interfaces;
using DataAccess.Models;
using Microsoft.AspNetCore.Mvc;
using WebAPI.DTOs;
using WebAPI.DTOs.Converters;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OnlineDataController : ControllerBase
    {
        IDataRowRepository _dataRowRepository;


        public OnlineDataController(IDataRowRepository dataRowRepository)
        {
            _dataRowRepository = dataRowRepository;
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<IEnumerable<DataRowDto>>> GetAllByIdAsync(int id)
        {
            var rows = await _dataRowRepository.GetByIdAsync(id);
            var rowDtos = DtoConverter<DataRow, DataRowDto>.FromList(rows);
            return Ok(rowDtos);
        }

        [HttpGet("{id:int},{time}")]
        public async Task<ActionResult<IEnumerable<DataRowDto>>> GetByIdAfterTime(int id, string time)
        {
            var allRows = await _dataRowRepository.GetByIdAsync(id);
            var filteredRows = allRows.Where(row => row.TimeUTC > DateTime.Parse(time));
            var rowDtos = DtoConverter<DataRow, DataRowDto>.FromList(filteredRows);
            return Ok(rowDtos);
        }
    }
}
