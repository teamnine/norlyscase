from cmath import nan
from urllib import response
from bokeh.driving import count
from bokeh.models import ColumnDataSource,Panel, Tabs, Button, DatetimeTickFormatter, Select
from bokeh.events import ButtonClick
from bokeh.plotting import curdoc, figure
from bokeh.colors import RGB
import requests
import datetime
from time import sleep, strftime


update_interval = 1000
roll_over = 200
source = ColumnDataSource({'x':[], 'y':[]})
source2 = ColumnDataSource({'x':[], 'y':[]})
source3 = ColumnDataSource({'x':[], 'y':[]})

active_color = RGB(64, 64, 64)
forecast_color = RGB(214, 162, 173)
KNN_color = RGB(25,132,163)
#these are all global variables used in functions
asset_id = 47
#fig = figure(width=1200, height=600)
fig = figure(width=1200, height=500, sizing_mode = "stretch_width")
plot_line = fig.line('x', 'y', source=source, color=active_color, line_width = 2, legend_label="Real data")
plot_line2 = fig.line('x', 'y', source=source2, color=forecast_color, line_width = 2, legend_label="Forecast")
plot_line3 = fig.line('x', 'y', source=source3, color=KNN_color, line_width = 2, legend_label="KNN")
fig.xaxis.axis_label = 'Time UTC'
fig.yaxis.axis_label = 'Active Power (Watt)'
fig.legend.label_text_font_style = "bold"
fig.legend.label_text_font_size = "16px"
fig.legend.location = "top_left"
 
fig.xaxis.formatter = DatetimeTickFormatter(
    days=["%m/%d %H:%M"],
    months=["%m/%d %H:%M"],
    hours=["%m/%d %H:%M"],
    minutes=["%m/%d %H:%M"]
)

data = {'x':[], 'y':[]}
data_forecast =  {'x':[], 'y':[]}
data_knn = {'x':[], 'y':[]}
is_predicted = False

last_date = None
last_forecast_date = None
last_knn_date = None

def get_asset_all_data():
    global asset_id, data, last_date
    response = requests.get(f"http://localhost:5207/api/OnlineData/{asset_id}")
    try:
        if len(response.json()) == 0:
            return
        r = response.json()
        data = {'x':[], 'y':[]}
        for i, row in enumerate(r):
            #data.append({'x': [datetime.datetime.strptime(row['timeUTC'].split('.')[0], "%Y-%m-%dT%H:%M:%S")], 'y': [row['activePowerWatt']]})
            data['x'].append(datetime.datetime.strptime(row['timeUTC'].split('.')[0], "%Y-%m-%dT%H:%M:%S"))
            data['y'].append(row['activePowerWatt'])
        source.stream(data, rollover=roll_over)
        last_date = data['x'][-1]
    except:
        print("ERRO")
        pass

def get_asset_new_data():
    global asset_id
    global data
    global data_forecast
    global data_knn
    global is_predicted
    global last_date, last_forecast_date, last_knn_date
    
    time_utc = last_date
    print(time_utc)
    response = requests.get(f"http://localhost:5207/api/OnlineData/{asset_id},{time_utc.strftime('%Y-%m-%dT%H:%M:%S')}")
    for i, row in enumerate(response.json()):
        row_time = datetime.datetime.strptime(row['timeUTC'].split('.')[0], "%Y-%m-%dT%H:%M:%S")
        if row['activePowerWatt'] > 0:
            data['x'].append(row_time)
            data['y'].append(row['activePowerWatt'])

        if row['predictedActivePowerWatt'] > 0:
            if last_forecast_date is None or row_time > last_forecast_date:
                print('it is None')
                data_forecast['x'].append(row_time)
                data_forecast['y'].append(row['predictedActivePowerWatt'])

        if row['predictedActivePowerWattKNN'] > 0:
            if last_knn_date is None or row_time > last_knn_date:
                data_knn['x'].append(row_time)
                data_knn['y'].append(row['predictedActivePowerWattKNN'])
    
    if len(data['x']) > 1:
        source.stream(data, rollover=roll_over)
        last_date = data['x'][-1]
    
    if len(data_forecast['x']) > 0 and data_forecast['x'][-1] != last_forecast_date:
        source2.stream(data_forecast, rollover=roll_over)
        last_forecast_date = data_forecast['x'][-1]
    if len(data_knn['x']) > 0 and data_knn['x'][-1] != last_knn_date:
        source3.stream(data_knn, rollover=roll_over)
        last_knn_date = data_knn['x'][-1]
    
    data = {'x':[], 'y':[]}
    data_forecast = {'x':[], 'y':[]}
    data_knn = {'x':[], 'y':[]}

def select_asset(attr, old, new):
    global asset_id
    global source
    asset_id = new
    source.data = {'x':[], 'y':[]}
    get_asset_all_data()
    plot_line = fig.line('x', 'y', source=source, color=active_color)

get_asset_all_data()

select = Select(title="Select wind turbine:", value=str(asset_id), options=["47", "49", "50", "54", "93", "94", "96", "97"])
select.on_change('value', select_asset)

""" def callback_for_stranger_button(event):
    global data_bool
    data_bool = not data_bool

button = Button(label='Hi yes Starger!', button_type='danger')
button.on_event(ButtonClick, callback_for_stranger_button) """

doc = curdoc()

doc.add_root(select)
doc.add_root(fig)
doc.add_periodic_callback(get_asset_new_data, update_interval)

import os
os.system(f'bokeh serve --show {__file__}')
