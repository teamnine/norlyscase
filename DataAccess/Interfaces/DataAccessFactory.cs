﻿using DataAccess.Repositories;
using System;

namespace DataAccess.Interfaces;
public class DataAccessFactory
{
    public static T GetDataAccess<T>(string connectionString) where T : class
    {
        switch (typeof(T).Name)
        {
            case "IDataRowRepository": return new DataRowRepository(connectionString) as T;
            case "IAssetRepository": return new AssetRepository(connectionString) as T;
            default:
                break;
        }
        throw new ArgumentException($"Unknown type {typeof(T).FullName}");
    }
}