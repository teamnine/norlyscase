﻿using DataAccess.Models;

namespace DataAccess.Interfaces;
public interface IAssetRepository : IBaseRepository<Asset> { }
