﻿using DataAccess.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccess.Interfaces;
public interface IDataRowRepository
{
    Task<int> CreateAsync(DataRow entity);
    Task<IEnumerable<DataRow>> GetByIdAsync(int id);
    Task<bool> UpdateAsync(DataRow entity);
    Task<IEnumerable<DataRow>> GetAllAsync();
}