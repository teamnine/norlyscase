﻿using Dapper;
using DataAccess.Interfaces;
using DataAccess.Models;
using System;
using System.Threading.Tasks;

namespace DataAccess.Repositories;
public class AssetRepository : BaseRepository<Asset>, IAssetRepository
{
    public AssetRepository(string connectionstring) : base(connectionstring)
    {
        TableName = "Assets";
    }

    public override async Task<int> CreateAsync(Asset entity)
    {
        string command = $"INSERT INTO [{TableName}] VALUES (@Id, @Neighbor1, @Neighbor2, @Neighbor3);";
        try
        {
            using var connection = CreateConnection();
            if (await connection.ExecuteAsync(command, entity) > 0)
            {
                return entity.Id;
            }    
            return -1;
        }
        catch (Exception ex)
        {
            throw new Exception($"Error during async creation of 'Assets'!\nMessage was: '{ex.Message}'\nTable Name: {TableName}\nCommand: {command}", ex);
        }
    }
}