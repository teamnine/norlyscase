﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using System;
using DataAccess.Interfaces;

namespace DataAccess.Repositories;
public abstract class BaseRepository<T> : IBaseRepository<T> where T : class
{
    private readonly string _connectionstring;
    private string ValueNames => string.Join(", ", Values.ToList().Select(property => property.Trim()));
    private string ValueParameters => string.Join(", ", RawValues.ToList().Select(property => $"@{property.Trim()}"));
    private string ValueUpdates => string.Join(", ", RawValues.ToList().Select(property => $"{property.Trim()}=@{property.Trim()}"));

    protected BaseRepository(string connectionstring)
    {
        TableName = typeof(T).Name;
        RawValues = typeof(T).GetProperties().Where(property => property.Name != "Id").Select(property => property.Name);
        Values = RawValues;
        _connectionstring = connectionstring;
    }
    protected IDbConnection CreateConnection() => new SqlConnection(_connectionstring);
    protected string TableName { get; set; }
    protected IEnumerable<string> Values { get; set; }
    protected IEnumerable<string> RawValues;

    public virtual async Task<int> CreateAsync(T entity)
    {
        string command = $"INSERT INTO [{TableName}] ({ValueNames}) OUTPUT INSERTED.Id VALUES ({ValueParameters});";
        try
        {
            using var connection = CreateConnection();
            return await connection.QuerySingleAsync<int>(command, entity);
        }
        catch (Exception ex)
        {
            throw new Exception($"Error during async creation of '{typeof(T).Name}'!\nMessage was: '{ex.Message}'\nTable Name: {TableName}\nValueNames: {ValueNames}\nCommand: {command}", ex);
        }
    }

    public virtual async Task<IEnumerable<T>> GetAllAsync()
    {
        string command = $"SELECT * FROM [{TableName}];";
        try
        {
            using var connection = CreateConnection();
            return await connection.QueryAsync<T>(command);
        }
        catch (Exception ex)
        {
            throw new Exception($"Error getting all async of '{typeof(T).Name}'!\nMessage was: '{ex.Message}'\nTable Name: {TableName}\nValueNames: {ValueNames}\nCommand: {command}", ex);
        }
    }

    public virtual async Task<T> GetByIdAsync(int id)
    {
        string command = $"SELECT * FROM [{TableName}] WHERE Id=@Id;";
        try
        {
            using var connection = CreateConnection();
            return await connection.QueryFirstOrDefaultAsync<T>(command, new { Id = id });
        }
        catch (Exception ex)
        {
            throw new Exception($"Error getting asyng by Id:`{id}` of '{typeof(T).Name}'!\nMessage was: '{ex.Message}'\nTable Name: {TableName}\nValueNames: {ValueNames}\nCommand: {command}", ex);
        }
    }

    public virtual async Task<bool> DeleteAsync(int id)
    {
        string command = $"DELETE FROM [{TableName}] WHERE Id=@Id;";
        try
        {
            using var connection = CreateConnection();
            return await connection.ExecuteAsync(command, new { Id = id }) > 0;
        }
        catch (Exception ex)
        {
            throw new Exception($"Error during async deletion of '{typeof(T).Name}' with Id:{id}!\nMessage was: '{ex.Message}'\nTable Name: {TableName}\nCommand: {command}", ex);
        }
    }

    public virtual async Task<bool> UpdateAsync(T entity)
    {
        string command = $"UPDATE [{TableName}] SET {ValueUpdates} WHERE Id=@Id;";
        try
        {
            using var connection = CreateConnection();
            return await connection.ExecuteAsync(command, entity) > 0;
        }
        catch (Exception ex)
        {
            throw new Exception($"Error during async update of '{typeof(T).Name}'!\nMessage was: '{ex.Message}'\nTable Name: {TableName}\nValueNames: {ValueNames}\nCommand: {command}", ex);
        }
    }
}
