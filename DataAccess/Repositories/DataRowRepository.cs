﻿using Dapper;
using DataAccess.Interfaces;
using DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DataAccess.Repositories;

public class DataRowRepository : IDataRowRepository
{
    private readonly string _connectionstring;
    protected string TableName { get; set; }

    public DataRowRepository(string connectionstring)
    {
        TableName = "OnlineData";
        _connectionstring = connectionstring;
    }
    protected System.Data.IDbConnection CreateConnection() => new SqlConnection(_connectionstring);

    public async Task<int> CreateAsync(DataRow entity)
    {
        string command = $"INSERT INTO [{TableName}] VALUES (@AssetId, @TimeUTC, @WindSpeedMs, @WindDirectionDegrees, @Temperature, @ActivePowerWatt, @PredictedActivePowerWatt, @PredictedActivePowerWattKNN);";
        try
        {
            using var connection = CreateConnection();
            return await connection.ExecuteAsync(command, entity);
        }
        catch (Exception ex)
        {
            throw new Exception($"Error during async creation of 'DataRow'!\nMessage was: '{ex.Message}'\nTable Name: {TableName}\nCommand: {command}", ex);
        }
    }

    public async Task<IEnumerable<DataRow>> GetByIdAsync(int id)
    {
        string command = $"SELECT * FROM [{TableName}] WHERE AssetId=@AssetId ORDER BY TimeUTC ASC;";
        try
        {
            using var connection = CreateConnection();
            return await connection.QueryAsync<DataRow>(command, new { AssetId = id });
        }
        catch (Exception ex)
        {
            throw new Exception($"Error getting asyng by AssetId:`{id}` of 'DataRow'!\nMessage was: '{ex.Message}'\nCommand: {command}", ex);
        }
    }

    public async Task<bool> UpdateAsync(DataRow entity)
    {
        string command = $"UPDATE [{TableName}] SET WindSpeedMs=@WindSpeedMs, WindDirectionDegrees=@WindDirectionDegrees, Temperature=@Temperature, ActivePowerWatt=@ActivePowerWatt, PredictedActivePowerWatt=@PredictedActivePowerWatt, PredictedActivePowerWattKNN=@PredictedActivePowerWattKNN WHERE AssetId=@AssetId AND TimeUTC=@TimeUTC;";
        try
        {
            using var connection = CreateConnection();
            return await connection.ExecuteAsync(command, entity) > 0;
        }
        catch (Exception ex)
        {
            throw new Exception($"Error during async update of 'DataRow'!\nMessage was: '{ex.Message}'\nTable Name: {TableName}\nCommand: {command}", ex);
        }
    }

    public async Task<IEnumerable<DataRow>> GetAllAsync()
    {
        string command = $"SELECT * FROM [{TableName}];";
        try
        {
            using var connection = CreateConnection();
            return await connection.QueryAsync<DataRow>(command);
        }
        catch (Exception ex)
        {
            throw new Exception($"Error getting all async of 'DataRow'!\nMessage was: '{ex.Message}'\nTable Name: {TableName}\nCommand: {command}", ex);
        }
    }
}
