using DataAccess.Models;
using DataAccess.Repositories;
using NUnit.Framework;
using System.Threading.Tasks;

namespace Tests.DataAccessTest
{
    public class AssetTest
    {
        private AssetRepository _assetRepository;
        private int _leastCreatedAssetId;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _assetRepository = new AssetRepository(Configuration.CONNECTION_STRING_TEST);
        }

        [Test]
        [Order(1)]
        public async Task CreateAssetTest()
        {
            //Arrange
            Asset asset = new() {Id = 0, Neighbor1 = 1, Neighbor2 = 2, Neighbor3 = 3 };

            //Act
            asset.Id = await _assetRepository.CreateAsync(asset);
            Asset newAsset = await _assetRepository.GetByIdAsync(asset.Id);
            _leastCreatedAssetId = asset.Id;

            //Assert
            Assert.AreEqual(asset.Id, newAsset.Id, $"Asset doesn't correspond to the expected Id: {newAsset.Id}");
        }

        [Test]
        [Order(2)]
        public async Task GetAssetByIdTest()
        {
            //Arrange
            //Act
            Asset newAsset = await _assetRepository.GetByIdAsync(_leastCreatedAssetId);

            //Assert
            Assert.AreEqual(_leastCreatedAssetId, newAsset.Id, $"Asset doesn't correspond to the expected Id: {_leastCreatedAssetId}");
        }

        [Test]
        [Order(3)]
        public async Task UpdateAssetByTest()
        {
            //Arrange
            Asset newAsset = new() { Id = _leastCreatedAssetId, Neighbor1 = 2, Neighbor2 = 3, Neighbor3 = 4 };
            //Act
            bool updateStatus = await _assetRepository.UpdateAsync(newAsset);

            //Assert
            Assert.IsTrue(updateStatus, $"Asset doesn't correspond to the expected Id: {_leastCreatedAssetId}");
        }

        [Test]
        [Order(4)]
        public async Task DeleteAssetByIdTest()
        {
            //Arrange
            //Act
            bool deletionResult = await _assetRepository.DeleteAsync(_leastCreatedAssetId);

            //Assert
            Assert.IsTrue(deletionResult, $"Asset doesn't correspond to the expected Id: {_leastCreatedAssetId}");
        }
    }
}