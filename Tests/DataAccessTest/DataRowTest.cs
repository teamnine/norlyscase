using DataAccess.Models;
using DataAccess.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tests.DataAccessTest
{
    public class DataRowTest
    {
        private DataRowRepository _dataRowRepository;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _dataRowRepository = new DataRowRepository(Configuration.CONNECTION_STRING_TEST);
        }

        [Order(1)]
        [Test]
        public async Task CreatingADataRowReturns1TupleUpdated()
        {
            DateTime now = DateTime.Now;
            DateTime endDate = now - new TimeSpan(0, 0, 0, 1, now.TimeOfDay.Milliseconds);
            //Arrange
            DataRow dataRow = new() { AssetId = 1, TimeUTC = endDate, WindSpeedMs = 22, WindDirectionDegrees = 145, Temperature = 8, ActivePowerWatt = 1500000, PredictedActivePowerWatt = 0, PredictedActivePowerWattKNN = 0 };
            //Act
            int rowsAffected = await _dataRowRepository.CreateAsync(dataRow);
            //Assert
            Assert.AreEqual(1, rowsAffected, $"Created DataRow affected '{rowsAffected}' rows instead of 1");
        }

        [Order(2)]
        [Test]
        public async Task UpdatingADataRowSuccessfullyChangesTheValues()
        {
            //Arrange
            DateTime now = DateTime.Now;
            DateTime endDate = now - new TimeSpan(0, 0, 0, 0, now.TimeOfDay.Milliseconds);
            DataRow dataRow = new() { AssetId = 1, TimeUTC = endDate, WindSpeedMs = 23, WindDirectionDegrees = 127, Temperature = 3, ActivePowerWatt = 1500000, PredictedActivePowerWatt = 0, PredictedActivePowerWattKNN = 0 };
            //Act
            await _dataRowRepository.CreateAsync(dataRow);
            dataRow.PredictedActivePowerWatt = 1435831;
            bool updateStatus = await _dataRowRepository.UpdateAsync(dataRow);
            DataRow updatedDataRow = (await _dataRowRepository.GetByIdAsync(1)).Last();
            //Assert
            Assert.IsTrue(updateStatus, $"Row was not updated successfully");
            Assert.AreEqual(1435831, updatedDataRow.PredictedActivePowerWatt, $"PredictedActivePower was updated to '{updatedDataRow.PredictedActivePowerWatt}' instead of 1435831");
        }

        [Order(3)]
        [Test]
        public async Task GettingAllDataRowsReturnsListOfRowsBiggerThan0()
        {
            //Arrange
            //Act
            IEnumerable<DataRow> allDataRows = await _dataRowRepository.GetAllAsync();
            //Assert
            Assert.IsTrue(allDataRows.Any(), "List of DataRows is currently of size 0");
        }

        [Order(4)]
        [Test]
        public async Task GettingDataRowsById1ReturnsTheRows()
        {
            //Arrange
            //Act
            IEnumerable<DataRow> allDataRowsForId = await _dataRowRepository.GetByIdAsync(1);

            //Assert
            Assert.IsTrue(allDataRowsForId.Any(), $"No DataRow found by the AssetId 1");
            int id = allDataRowsForId.First().AssetId;
            Assert.IsTrue(id == 1, $"The AssetId was not 1, it was {id}.");
        }
    }
}