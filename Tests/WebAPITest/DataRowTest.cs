﻿using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using StubsClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.Controllers;
using WebAPI.DTOs;

namespace Tests.WebAPITest
{
    internal class DataRowTest
    {
        private OnlineDataController _testDataRowApi;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _testDataRowApi = new OnlineDataController(new DataRowStub());
        }

        [Test]
        public async Task GettingAllByIdReturnsAListBiggerThan0()
        {
            //Arrange - in one time setup
            //Act
            var actionResult = (await _testDataRowApi.GetAllByIdAsync(1)).Result;

            if (actionResult is ObjectResult objRes)
            {
                //Assert
                Assert.AreEqual(200, objRes.StatusCode, "The returned status code was not 200.");

                IEnumerable<DataRowDto> assets = (IEnumerable<DataRowDto>)objRes.Value;
                Assert.IsTrue(assets.Any(), "The list was empty.");
            }

            else if (actionResult is StatusCodeResult scr)
            {
                //Assert
                Assert.AreEqual(200, scr.StatusCode, "The status code was not 200.");
            }
        }

        [Test]
        public async Task GettingByIdAndTimeReturnsListBiggerThan0AfterTime()
        {
            //Arrange - in one time setup
            string time = "2022-01-01";

            //Act
            var actionResult = (await _testDataRowApi.GetByIdAfterTime(1, time)).Result;

            if (actionResult is ObjectResult objRes)
            {
                //Assert
                Assert.AreEqual(200, objRes.StatusCode, "The returned status code was not 200.");

                IEnumerable<DataRowDto> assets = (IEnumerable<DataRowDto>)objRes.Value;
                Assert.IsTrue(assets.Any(), "The list was empty.");
            }

            else if (actionResult is StatusCodeResult scr)
            {
                //Assert
                Assert.AreEqual(200, scr.StatusCode, "The status code was not 200.");
            }
        }
    }
}
