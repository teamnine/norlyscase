﻿using NUnit.Framework;
using WebAPI.Controllers;
using WebAPI.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StubsClassLibrary;
using Microsoft.AspNetCore.Mvc;
using DataAccess.Models;
using WebAPI;

namespace Tests.WebAPITest
{
    public class AssetTest
    {
        private AssetsController _testAssetApi;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _testAssetApi = new AssetsController(new AssetStub());
        }

        [Test]
        public async Task GettingAllAssetsReturnListBiggerThan0()
        {
            //Arrange - in one time setup
            //Act
            var actionResult = (await _testAssetApi.GetAllAsync()).Result;

            if (actionResult is ObjectResult objRes)
            {
                //Assert
                Assert.AreEqual(200, objRes.StatusCode, "The returned status code was not 200.");

                IEnumerable<AssetDto> assets = (IEnumerable<AssetDto>)objRes.Value;
                Assert.IsTrue(assets.Any(), "The list was empty.");
            }

            else if (actionResult is StatusCodeResult scr)
            {
                //Assert
                Assert.AreEqual(200, scr.StatusCode, "The status code was not 200.");
            }
        }

        [Test]
        public async Task GettingAssetById1()
        {
            //Arrange - in one time setup
            //Act
            var actionResult = (await _testAssetApi.GetByIdAsync(1)).Result;

            if (actionResult is ObjectResult objRes)
            {
                //Assert
                Assert.AreEqual(200, objRes.StatusCode, "The returned status code was not 200.");

                AssetDto asset = (AssetDto)objRes.Value;
                Assert.IsNotNull(asset, "The asset was null.");
            }

            else if (actionResult is StatusCodeResult scr)
            {
                //Assert
                Assert.AreEqual(200, scr.StatusCode, "The status code was not 200.");
            }
        }
    }
}
